<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Products;
use App\Models\User;
use Auth;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Products::query();

        // Mengambil parameter limit
        $limit = $request->input('limit');
        if ($limit) {
            $products->limit($limit);
        }

        // Mengambil parameter sortby dan orderby
        $sortby = $request->input('sortby');
        $orderby = $request->input('orderby');
        if ($sortby && $orderby) {
            $products->orderBy($sortby, $orderby);
        }

        $products = $products->get();

        return response()->json($products, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validasi apakah user merupakan admin atau bukan
    if (Auth::user()->role == 2) {
        return response()->json(['message' => 'Unauthorized'], 401);
    }

// Validasi data product
    $validatedData = $request->validate([
        'name' => 'required|string',
        'price' => 'required|numeric',
        'quantity' => 'required|integer',
    ]);

    // Simpan data product ke dalam database
    $product = new Products;
    $product->name = $validatedData['name'];
    $product->price = $validatedData['price'];
    $product->quantity = $validatedData['quantity'];
    // $product->user_id = Auth::id();
    $product->save();

    return response()->json(['message' => 'Product created successfully'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Products::findOrFail($id);

        return response()->json($product, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
           // Cari product berdasarkan ID
    $product = Products::findOrFail($id);

    // Validasi apakah user merupakan admin atau bukan
    if (Auth::user()->role == 2) {
        return response()->json(['message' => 'Unauthorized'], 401);
    }

    // Validasi data product
    $validatedData = $request->validate([
        'name' => 'required|string',
        'price' => 'required|numeric',
        'quantity' => 'required|integer',
    ]);

    // Update data product
    $product->name = $validatedData['name'];
    $product->price = $validatedData['price'];
    $product->quantity = $validatedData['quantity'];
    $product->save();

    return response()->json(['message' => 'Product updated successfully'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::user()->role == 2) {
            return response()->json(['message' => 'Unauthorized'], 401);
        }

        $product = Products::find($id);

    if (!$product) {
        return response()->json(['message' => 'Produk tidak ditemukan'], 404);
    }

    $product->delete();

    return response()->json(['message' => 'Produk berhasil dihapus'], 200);
    }
}
