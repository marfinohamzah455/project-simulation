<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use Validator;
use App\Models\Products;
class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Jika user yang melakukan request adalah Admin
        if (auth()->user()->role == 1) {
            $transactions = Transaction::query();
        }
        // Jika user yang melakukan request adalah Customer
        else {
            $transactions = Transaction::where('user_id', auth()->id());
        }

        // Filter berdasarkan parameter limit
        if ($request->has('limit')) {
            $transactions->limit($request->limit);
        }

        // Filter dan sorting berdasarkan parameter sortby dan orderby
        if ($request->has('sortby') && $request->has('orderby')) {
            $transactions->orderBy($request->sortby, $request->orderby);
        }

        $transactions = $transactions->get();
        // dd($transactions);
        return response()->json([
            'data' => $transactions,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validasi input
        $validator = Validator::make($request->all(), [
            'product_id' => 'required|exists:products,id',
            'quantity' => 'required|integer|min:1',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        // Ambil data product dari database
        $product = Products::findOrFail($request->input('product_id'));
        // Validasi stok barang
        if ($product->quantity < $request->input('quantity')) {
            return response()->json(['error' => 'Stok barang kosong'], 400);
        }

        // Hitung total harga pembelian
        $price = $product->price;
        $tax = $price * 0.1;
        $admin_fee = ($price + $tax) * 0.05;
        $total = ($price + $tax + $admin_fee) * $request->input('quantity');

        // Simpan data transaction ke dalam database
        $transaction = new Transaction;
        $transaction->user_id = $request->user()->id;
        $transaction->product_id = $request->input('product_id');
        $transaction->price = $price;
        $transaction->quantity = $request->input('quantity');
        $transaction->admin_fee = $admin_fee;
        $transaction->tax = $tax;
        $transaction->total = $total;
        $transaction->save();

        // Kurangi stok barang
        $product->quantity -= $request->input('quantity');
        $product->save();

        return response()->json($transaction, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (auth()->user()->role == 1) {
            $transaction = Transaction::find($id);
        } else {
            $transaction = Transaction::where('id', $id)->where('user_id', auth()->id())->first();
        }

        if (!$transaction) {
            return response()->json([
                'message' => 'Transaction not found',
            ], 404);
        }

        return response()->json([
            'data' => $transaction,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
